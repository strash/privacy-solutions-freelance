// menu
const menuLinks         = document.querySelector('.menu-links');
const subMenu           = document.querySelector('.menu-hover');
const subMenuOverlay    = document.querySelector('.menu-hover-overlay');
const menuM             = document.querySelector('.menu-m');
const burger            = document.querySelector('.menu-m-burger');
const logotypeM         = document.querySelector('.logotype-m');
const menuMSubmenu      = document.querySelector('.menu-m-submenu');
const menuMStickyBlocks = document.querySelectorAll('.menu-m-submenu-sticky');
// main page
const ouradvantagesLeft              = document.querySelector('.ouradvantages-left');
const ouradvantagesList              = document.querySelector('.ouradvantages-list');
const ouradvantagesRight             = document.querySelector('.ouradvantages-right');
const ouradvantagesRightImage        = document.querySelector('.ouradvantages-right-image');
const ouradvantagesRightImageCaption = document.getElementById('ouradvantages-right-image-caption');
const businessRegCards               = document.querySelector('.businessregistration .cards');



function isTablet() {
  if (document.documentElement.clientWidth <= 1024) return true;
  return false;
}
function isMobile() {
  if (document.documentElement.clientWidth <= 667) return true;
  return false;
}


// submenu
let submenuCount = 0; // счетчик соответствия по data-menu-id

// отображение сабменю
function showSubMenu({ target }) {
  submenuCount = 0;
  let parent = target;
  while (parent.tagName !== 'A' && !parent.isSameNode(this)) parent = parent.parentNode;

  let i = 0;
  for (i; i < subMenu.children[0].children[0].children.length; i++) {
    if (subMenu.children[0].children[0].children[i].dataset.menuId == parent.dataset.menuId) {
      subMenu.children[0].children[0].children[i].style.display = 'grid';
      submenuCount++;
    } else subMenu.children[0].children[0].children[i].style.display = 'none';
  }

  if (submenuCount > 0) {
    subMenu.style.display = 'flex';
    subMenuOverlay.style.display = 'block';
  } else {
    subMenu.style.display = '';
    subMenuOverlay.style.display = '';
  }

  let j = 0;
  for (j; j < menuLinks.children.length; j++) {
    if (menuLinks.children[j] == parent) menuLinks.children[j].children[1].style.display = 'block';
    else menuLinks.children[j].children[1].style.display = '';
  }
}

// helper
function _destroySubMenu() {
  subMenu.style.display = '';
  subMenuOverlay.style.display = '';
  submenuCount = 0;

  let i = 0;
  for (i; i < menuLinks.children.length; i++) menuLinks.children[i].children[1].style = '';
}

// закрытие сабменю
function closeSubMenu({ pageY }) {
  const target       = this;
  const curY         = pageY - document.documentElement.scrollTop;

  if (target.classList.contains('menu-links') && curY < 96 ||
      target.classList.contains('menu-links') && curY >= 96 && submenuCount == 0) {
        _destroySubMenu();
  }
}

if (subMenu && !isTablet()) {
  menuLinks.addEventListener('mouseover', showSubMenu);
  menuLinks.addEventListener('mouseout', closeSubMenu);
  subMenuOverlay.addEventListener('mouseenter', _destroySubMenu);
}



// menu mobile
let menuMState = false; // true - opened, false - closed
function transformBurger() {
  menuMState = !menuMState;
  if (menuMState) {
    menuMSubmenu.style.display = 'block';
    this.children[0].children[0].style.transform = 'rotate(45deg) translate(9px, -16px)';
    this.children[0].children[1].style.opacity = '0';
    this.children[0].children[2].style.transform = 'rotate(-45deg) translate(-24px, 1px)';
    logotypeM.textContent = 'Menu';
    document.body.style.overflow = 'hidden';
  } else {
    menuMSubmenu.style.display = '';
    this.children[0].children[0].removeAttribute('style');
    this.children[0].children[1].removeAttribute('style');
    this.children[0].children[2].removeAttribute('style');
    logotypeM.textContent = 'Privacy Solutions';
    document.body.style.overflow = '';
  }
}
burger.addEventListener('click', transformBurger);

// menu mobile scroll
let vector = 0;
function showMenuM() {
  if (menuMState) return;
  setTimeout(() => vector = document.documentElement.scrollTop, 400);
  const scroll = document.documentElement.scrollTop;

  if (scroll >= 80 && vector < scroll) menuM.style.transform = 'translateY(-200px)';
  else menuM.style.transform = '';
}
window.addEventListener('scroll', showMenuM);

// menu mobile submenu
function initMenuMSubmenu() {
  function toggleSub() {
    if (this.children[1] && getComputedStyle(this.children[1]).display == 'none') {
      this.children[0].children[this.children[0].children.length - 1].style.transform = 'rotate(90deg)';
      this.children[1].style.display = 'block';
    } else {
      this.children[0].children[this.children[0].children.length - 1].style.transform = '';
      this.children[1].style.display = '';
    }
  }

  let i = 0;
  for(i; i < menuMStickyBlocks.length; i++) {
    menuMStickyBlocks[i].addEventListener('click', toggleSub);
  }
}
initMenuMSubmenu();




// our advantages list
function placeImage() {
  const activeItem = document.querySelector('.ouradvantages-list li.active');
  if (!activeItem) return;

  ouradvantagesRight.style.display = '';

  const scroll          = document.documentElement.scrollTop;
  const listTop         = ouradvantagesLeft.getBoundingClientRect().top + scroll;
  const activeItemProps = activeItem.getBoundingClientRect();
  const imageHeight     = ouradvantagesRight.getBoundingClientRect().height;
  const paddingT        = parseInt(getComputedStyle(activeItem).paddingTop);
  const paddingB        = parseInt(getComputedStyle(activeItem).paddingBottom);

  activeItem.style.paddingBottom = `${paddingT + imageHeight + paddingB}px`;
  ouradvantagesRight.style.top   = `${(activeItemProps.top + scroll - listTop) + activeItemProps.height}px`;
}

function toggleAdvantages({ target }) {
  let parent = target;
  while (parent.tagName !== 'LI' && !parent.isSameNode(this)) parent = parent.parentNode;

  if (isMobile() && target.classList.contains('active')) {
    target.removeAttribute('class');
    target.removeAttribute('style');
    ouradvantagesRight.style.display = 'none';
    return;
  }

  let activeI = 0;
  let i = 0;
  for (i; i < ouradvantagesList.children.length; i++) {
    if (ouradvantagesList.children[i].isSameNode(parent)) activeI = i;

    ouradvantagesList.children[i].removeAttribute('class');
    ouradvantagesList.children[i].removeAttribute('style');
    if (isMobile()) ouradvantagesRightImage.children[i].removeAttribute('style');
  }

  parent.classList.add('active');
  ouradvantagesRightImage.children[activeI].style.opacity = 1;
  ouradvantagesRightImageCaption.textContent = ouradvantagesRightImage.children[activeI].getAttribute('alt');

  if (isMobile()) placeImage();
}

if (ouradvantagesList) {
  ouradvantagesList.addEventListener('click', toggleAdvantages);

  if (isMobile()) {
    // при первой загрузке сворачиваем список
    document.querySelector('.ouradvantages-list li.active').removeAttribute('class');
    ouradvantagesRight.style.display = 'none';
    placeImage();
  }
  // HACK: сворачивать список при повороте экрана или при растягивании окна
  window.addEventListener('resize', () => {
    // если мобилка, то сворачиваем
    if (isMobile()) {
      ouradvantagesRight.style.display = 'none';
      document.querySelector('.ouradvantages-list li.active').removeAttribute('style');
      document.querySelector('.ouradvantages-list li.active').removeAttribute('class');
      document.querySelector('.ouradvantages-list').children[0].removeAttribute('class');
    // если больше, то по ситуации
    } else {
      const active = document.querySelector('.ouradvantages-list li.active');
      // если не было активного, то назначаем первый в списке
      if (!active) document.querySelector('.ouradvantages-list').children[0].className = 'active';
      ouradvantagesRight.removeAttribute('style');
      active.removeAttribute('style');
    }
  });
}



// businnes cards show more
if (businessRegCards) {
  const maxCardsWhenClose = 6;
  let   isCardsOpen       = false;

  const toggleCards = () => {
    let i = 0;
    for (i; i < businessRegCards.children.length; i++) {
      if (isCardsOpen) {
        businessRegCards.children[i].style.display = '';
        businessRegCards.nextElementSibling.textContent = 'close';
      } else if (!isCardsOpen && i + 1 > maxCardsWhenClose) {
        businessRegCards.children[i].style.display = 'none';
        businessRegCards.nextElementSibling.textContent = 'show more';
      }
    }
    isCardsOpen = !isCardsOpen;
  };

  toggleCards();

  businessRegCards.nextElementSibling.addEventListener('click', toggleCards);
}